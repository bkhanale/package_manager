import os
from platform import system


def pytest_configure(config):
    if os.environ.get('CI'):
        if system() == 'Windows':
            requirements_dir = 'dependency_management/requirements'
            os.remove('%s/AptRequirement.py' % requirements_dir)
            os.remove('%s/BrewRequirement.py' % requirements_dir)
            os.remove('%s/PkgRequirement.py' % requirements_dir)
            os.remove('%s/PortageRequirement.py' % requirements_dir)
            os.remove('%s/ZypperRequirement.py' % requirements_dir)
            os.remove('%s/DnfRequirement.py' % requirements_dir)
            os.remove('%s/YumRequirement.py' % requirements_dir)
            os.remove('%s/RscriptRequirement.py' % requirements_dir)

            os.remove('tests/AptRequirementTest.py')
            os.remove('tests/BrewRequirementTest.py')
            os.remove('tests/PkgRequirementTest.py')
            os.remove('tests/PortageRequirementTest.py')
            os.remove('tests/ZypperRequirementTest.py')
            os.remove('tests/DnfRequirementTest.py')
            os.remove('tests/YumRequirementTest.py')
            os.remove('tests/RscriptRequirementTest.py')
