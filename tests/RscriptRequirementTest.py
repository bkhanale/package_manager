import shutil
import unittest
import re

from sarge import get_stdout

from dependency_management.requirements.RscriptRequirement import (
    RscriptRequirement,
    VERSION_MATCHER,
    )


@unittest.skipIf(shutil.which('R') is None, "R is not installed.")
class RscriptRequirementTestCase(unittest.TestCase):

    def test__str__(self):
        self.assertEqual(str(RscriptRequirement('base')), 'base')

    def test_installed_requirement(self):
        self.assertTrue(RscriptRequirement('base').is_installed())

    def test_not_installed_requirement(self):
        self.assertFalse(RscriptRequirement('some_bad_package').is_installed())

    def test_remotes_install_version(self):
        requirement = RscriptRequirement('memoise', version='0.2.1', flag='-e',
                                         repo='http://cran.rstudio.com')
        self.assertEqual(requirement.install_package(), 0)
        self.assertTrue(requirement.is_installed())
        self.assertTrue(requirement.get_installed_version(), '0.2.1')

    def test_get_installed_version(self):
        """
        Tests if the current version of an R package can be extracted.

        Gets the currently installed R version and
        matches it with the extracted version of 'base' package
        """
        version_command_R = 'R -e version$version.string'
        output = get_stdout(version_command_R)
        version_string_dirty = re.split("\[1\]", output)[1]
        version_R = VERSION_MATCHER.findall(version_string_dirty)[0]
        requirement = RscriptRequirement('base')
        self.assertIsNotNone(requirement.get_installed_version())
        self.assertEqual(requirement.get_installed_version(), version_R)

    def test_get_installed_version_not_installed(self):
        requirement = RscriptRequirement('some_bad_package')
        self.assertFalse(requirement.is_installed())
        self.assertIsNone(requirement.get_installed_version())
